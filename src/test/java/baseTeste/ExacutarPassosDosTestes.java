package baseTeste;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class ExacutarPassosDosTestes {
    DadosLogin login = new DadosLogin();

    WebDriver browser  = new ChromeDriver();

    String numeroDaConta;
    String numeroDigito;

    @BeforeAll
    public void abrirSite() {

        browser.navigate().to("https://bugbank.netlify.app/");
        browser.manage().window().maximize();


    }
    public void chamadaRegistro() {
        abrirSite();
        cadastrarInformacoesUsuarioUm();
        browser.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
        cadastrarInformacoesUsuarioDois();

        //acessarAreaTransferencia();

    }

    public void cadastrarInformacoesUsuarioUm(){

        DadosCadastro dadosCadastro = new DadosCadastro();
        browser.findElement(dadosCadastro.botaoRegistrar).click();

        browser.findElement(dadosCadastro.campoEmailCadastro).sendKeys("joana.santos@gmail.com");
        browser.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro.campoNomeCadastro).sendKeys("Joana Santos");
        browser.findElement(dadosCadastro.campoSenhaCadastro).sendKeys("teste123");
        browser.findElement(dadosCadastro.campoConfirmaSenhaCadastro).sendKeys("teste123");

        browser.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro.labelSaldo).click();
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro.botaoCadastrar).click();
        browser.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro.botaoFechar).click();


        browser.findElement(login.campoEmail).sendKeys("joana.santos@gmail.com");
        browser.findElement(login.campoSenha).sendKeys("teste123");
        browser.findElement(login.botaoAvancar).click();
        extrairNumeroDaFrase();
        browser.findElement(login.botaoLogout).click();


    }
    private void extrairNumeroDaFrase() {

        DadosDaConta numero = new DadosDaConta();

        String texto = browser.findElement(numero.numeroConta).getText();
        String regex = "(\\d{3})-(\\d{1})";

        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
        java.util.regex.Matcher matcher = pattern.matcher(texto);


        if (matcher.find()) {
             numeroDaConta = matcher.group();
             System.out.println("Numero extraído: " + numeroDaConta);
        } else {
             System.out.println("não encontrado");

        }
    }

    public void cadastrarInformacoesUsuarioDois(){

        DadosCadastro dadosCadastro2 = new DadosCadastro();

        browser.findElement(dadosCadastro2.botaoRegistrar).click();

        browser.findElement(dadosCadastro2.campoEmailCadastro).clear();
        browser.findElement(dadosCadastro2.campoEmailCadastro).sendKeys("doninha.teste@gmail.com");
        browser.findElement(dadosCadastro2.campoNomeCadastro).clear();
        browser.findElement(dadosCadastro2.campoNomeCadastro).sendKeys("Doninha Silva");
        browser.findElement(dadosCadastro2.campoSenhaCadastro).clear();
        browser.findElement(dadosCadastro2.campoSenhaCadastro).sendKeys("123teste");
        browser.findElement(dadosCadastro2.campoConfirmaSenhaCadastro).clear();
        browser.findElement(dadosCadastro2.campoConfirmaSenhaCadastro).sendKeys("123teste");


        browser.findElement(dadosCadastro2.labelSaldo).click();
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro2.botaoCadastrar).click();
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro2.modal).isSelected();
        browser.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        browser.findElement(dadosCadastro2.botaoFechar).click();

        browser.findElement(login.campoEmail).sendKeys("doninha.teste@gmail.com");
        browser.findElement(login.campoSenha).sendKeys("123teste");
        browser.findElement(login.botaoAvancar).click();
        extrairNumeroDaFrase();
        //browser.findElement(login.botaoLogout).click();


    }


    public void acessarAreaTransferencia(){
        DadosTransferencia tranferencia = new DadosTransferencia();
        browser.findElement(tranferencia.botaoTranferencia).click();
        browser.findElement(tranferencia.campoNumeroConta).sendKeys(numeroDaConta);
        browser.findElement(tranferencia.campoDigito).sendKeys(numeroDigito);
        browser.findElement(tranferencia.campoValor).sendKeys("200");
        browser.findElement(tranferencia.campoDescricao).sendKeys("Tranferencia concluída");
        browser.findElement(tranferencia.botaoTranferirAgora).click();

        String fraseSucesso = browser.findElement(tranferencia.tranferenciaOk).getText();
        if(fraseSucesso.equals("Tranferencia realizada com sucesso")) {

            browser.findElement(tranferencia.botaoFechar).click();
        }

    }

}

